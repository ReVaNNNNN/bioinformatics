import random

Nucleotides = ["A", "C", "G", "T"]

def transcribe_DNA_int_RNA(sequence: str) -> str:
    return sequence.replace('T', 'U')


def get_random_dna_string(string_length: int):
    dna_string = ''
    for letter in range(string_length):
        dna_string += random.choice(Nucleotides)
    return dna_string


sequence = get_random_dna_string(40)
print(transcribe_DNA_int_RNA(sequence))