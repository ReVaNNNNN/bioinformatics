dna_nucleotides_map = {"A": "T", "C": "G", "G": "C", "T": "A"}


def reverse_complement(sequence: str) -> str:
    """Swapping Adenine with Thymine and Guanine with Cytosine. Reversing newly generated string"""
    result = ''
    for nucleotide in sequence:
            result += dna_nucleotides_map[nucleotide]

    return result[::-1]

dna = 'CTACGAGGAAGACCCGCGCTTTGGCCCCGCGTTGTAATCGAC'
print(reverse_complement(dna))