# DNA Toolkit file
import random
from structures import  *


# Check the sequence to make sure it is a DNA string
def validate_sequence(dna_sequence: str) -> bool:
    sequence = dna_sequence.upper()
    for nucleotide in sequence:
        if nucleotide not in nucleotides:
            return False
    return True


# Create a random DNA sequence
def get_random_dna_string(string_length: int):
    dna_string = ''
    for letter in range(string_length):
        dna_string += random.choice(nucleotides)
    return dna_string


def count_nucleotides_frequency(dna_sequence: str) -> dict:
    frequency = {"A": 0, "C": 0, "G": 0, "T": 0}
    for nucleotide in dna_sequence:
        frequency[nucleotide] += 1
    return frequency


def transcribe_dna_into_rna(sequence: str) -> str:
    """DNA -> RNA Transcription. Replacing Thymine with Uracil"""
    return sequence.replace('T', 'U')


def reverse_complement(sequence: str) -> str:
    """Swapping Adenine with Thymine and Guanine with Cytosine. Reversing newly generated string"""
    result = ''
    for nucleotide in sequence:
            result += dna_nucleotides_map[nucleotide]

    return result[::-1]
        


