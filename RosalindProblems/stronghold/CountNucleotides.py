def count_nucleotides_frequency(dna_sequence: str) -> dict:
    frequency = {"A": 0, "C": 0, "G": 0, "T": 0}
    for nucleotide in dna_sequence:
        frequency[nucleotide] += 1
    return frequency


def transform_result(frequency: dict) -> str:
    result = ''
    for key, val in frequency.items():
        result += (str(val)) + " "

    return result


#Place to sequence
dna_sequence = 'AAACGTTGCACGTACGATCGACTAGCGACTA'
frequency = count_nucleotides_frequency(dna_sequence)
print(transform_result(frequency))
