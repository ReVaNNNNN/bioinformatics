# DNA testing file
from DNAToolkit import *
from utilities import colored


string_length = int(input("Podaj długość łańcucha DNA: "))
random_dna_string = get_random_dna_string(string_length)
dna_string = get_random_dna_string(string_length)
# dna_string = ""

print('-' * 40)


def print_dna_information(sentence: str):
    print("Sekwencja DNA: " + colored(sentence))
    print('-' * 40)
    print("Długość selwencji : " + str(len(sentence)))
    print('-' * 40)
    print("Częstotliwość występowania nukleotydów: " + colored(str(count_nucleotides_frequency(sentence))))
    print('-' * 40)
    print("Sekwencja RNA: " + colored(str(transcribe_dna_into_rna(sentence))))
    print('-' * 40)
    print("Transkrypcja DNA: " + colored(str(reverse_complement(sentence))))
    print('-' * 40)
    print("5' " + colored(sentence) + " 3'")
    print('   ' + ''.join(['|' for sign in range(len(sentence))]))
    print("3' " + colored(str(reverse_complement(sentence)) + " 5'"))
    print('-' * 40)


if validate_sequence(dna_string):
    print_dna_information(dna_string)
else:
    print("Podany łańcuch DNA jest nieprawidłowy")
