# Solution Task 1
#import this

# Solution Task 2
def calculate_result(a: int, b: int) -> int:
    return a**2 + b**2

# print(calculate_result(877, 972))


# Solution Task 3
def cut_sentence(sentence: str, a: int, b: int, c: int, d: int) -> str:
    return sentence[a : b + 1] + " " + sentence[c : d + 1]

long = 'H83rn9LNYXM0rUAYOrssHzIxOLzHaLinbpDoPylLnj7MFDBRrahc6IQRYunk1uvMq0mrvbOUArenariauqL46zToq4eezqGEk7K81e632JmnyEWIQUVIfvvvbQn7iIoqKmGmsSMZ91H4pugatshukiMN3WLNg3vS1LiL7W7HK1ZTUnoWnId8dk..'
a = 72
b = 79
c = 140
d = 149
# print(cut_sentence(long, a, b, c, d))


# Solution Task 4
def sum_all_odd_numbers(start: int, end: int) -> int:
    sum = 0
    for val in range(start, end + 1):
        if val % 2 == 1:
            sum += val
    return sum

# print(sum_all_odd_numbers(4748, 9562))


# Solution Task 5
def return_even_line_from_file(file_path: str):
    file = open(file_path, 'r')
    result = open('result.txt', 'w')
    for number, line in enumerate(file.readlines()):
        if number % 2 == 1:
            result.write(line)
    file.close()


file_name = 'rosalind.txt'
return_even_line_from_file(file_name)


# Solution Task 6
def count_words_in_sentence(sentence: str) -> dict:
    result = {}
    for word in sentence.split(' '):
        if word in result:
            result[word] += 1
        else:
            result[word] = 1
    return result

def transform_result(sentence: dict) -> str:
    result = ''
    for item in sentence:
        result += f'{item} {sentence[item]}\n'
    return result

sentence = 'When I find myself in times of trouble Mother Mary comes to me Speaking words of wisdom let it be And in my hour of darkness she is standing right in front of me Speaking words of wisdom let it be Let it be let it be let it be let it be Whisper words of wisdom let it be And when the broken hearted people living in the world agree There will be an answer let it be For though they may be parted there is still a chance that they will see There will be an answer let it be Let it be let it be let it be let it be There will be an answer let it be Let it be let it be let it be let it be Whisper words of wisdom let it be Let it be let it be let it be let it be Whisper words of wisdom let it be And when the night is cloudy there is still a light that shines on me Shine until tomorrow let it be I wake up to the sound of music Mother Mary comes to me Speaking words of wisdom let it be Let it be let it be let it be yeah let it be There will be an answer let it be Let it be let it be let it be yeah let it be Whisper words of wisdom let it be'
print(transform_result(count_words_in_sentence(sentence)))

